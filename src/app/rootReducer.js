import { combineReducers } from 'redux';
import productListReducer from '../features/redux/reducer';

export default combineReducers({
    productList: productListReducer,
  }
);
