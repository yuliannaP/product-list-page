import { createStore as reduxCreateStore } from "redux";

import rootReducer from "./rootReducer";

const createStore = () => reduxCreateStore(rootReducer);
export default createStore;