import axios from 'axios';

import {updateProductList, insertNewProduct, updateProduct, removeProduct, errorState} from '../redux/actions';

const API = axios.create();

export const getProductList = () => API.get('/api/v1/products');

export const addProductToList = (product) => API.post('/api/v1/products', product);

export const putProduct = (product) => API.put(`/api/v1/products/${product.productId}`, product);

export const deleteProduct = (product) => API.delete(`/api/v1/products/${product.productId}`);

export const getProducts = async (dispatch) => {
    try {
        const products = await getProductList();
        dispatch(updateProductList(products.data));
    } catch (err) {
        dispatch(errorState());
    }
};

export const addProduct = async (dispatch, product) => {
    try {
        const products = await addProductToList(product);
        dispatch(insertNewProduct(products.data.content));
        return products;
    } catch (err) {
        dispatch(errorState());
    }
};

export const updateProductItem = async (dispatch, product) => {
    try {
        const products = await putProduct(product);
        dispatch(updateProduct(products.data.content));
    } catch (err) {
        dispatch(errorState());
    }
};

export const deleteProductItem = async (dispatch, product) => {
    try {
        await deleteProduct(product);
        dispatch(removeProduct(product));
    } catch (err) {
        dispatch(errorState());
    }
};