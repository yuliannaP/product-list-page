export const updateProductList = productList => ({type: 'UPDATE_LIST', payload: productList});

export const insertNewProduct = product => ({type: 'ADD_PRODUCT', payload: product});

export const updateProduct = product => ({type: 'UPDATE_PRODUCT', payload: product});

export const removeProduct = product => ({type: 'DELETE_PRODUCT', payload: product});

export const errorState = () => ({type: 'ERROR_STATE'});