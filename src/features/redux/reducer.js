const initialState = {productList: [], error: false};

const updateProductList = (state, action) => ({...state, productList: action.payload});
const addProduct = (state, action) => {
    state.productList.unshift(action.payload);
    const arr = state.productList.splice(0);
    return {...state, productList: arr};
};
const updateProduct = (state, action) => {
    const index = state.productList.findIndex((product => product.productId === action.payload.productId));
    state.productList[index] = action.payload;
    const arr = state.productList.splice(0);
    return {...state, productList: arr};
};

const deleteProduct = (state, action) => {
    const index = state.productList.findIndex((product => product.productId === action.payload.productId));
    state.productList.splice(index, 1);
    const arr = state.productList.splice(0);
    return {...state, productList: arr};
};

const errorState = (state) => {
    return {...state, productList: [], error: true};
}

const productListReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_LIST': return updateProductList(state, action);
        case 'ADD_PRODUCT': return addProduct(state, action);
        case 'UPDATE_PRODUCT': return updateProduct(state, action);
        case 'DELETE_PRODUCT': return deleteProduct(state, action);
        case 'ERROR_STATE': return errorState(state);
        default: return state;
    }
};
export default productListReducer;