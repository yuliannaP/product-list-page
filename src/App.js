import React from 'react';

import ProductListTable from './features/productList';

function App() {
  return (
    <div className="App">
      <ProductListTable />
    </div>
  );
}

export default App;
